var express = require('express');
var router = express.Router();
var officegen = require('officegen');
var fs = require('fs');
var path = require('path');
var async = require('async');

var outDir = path.join(__dirname, '../tmp/')

/* GET users listing. */
router.post('/', function(req, res, next) {

  var description = req.body.description;

  if(description){

    var docx = officegen({
      type: 'docx',
      orientation: 'portrait',
      pageMargins: { top: 1000, left: 1000, bottom: 1000, right: 1000 }
    });
  
    docx.on('error', function (err) {
      console.log(err)
    });
  
    var pObj = docx.createP();
  
    pObj.addText(description);
  
    var out = fs.createWriteStream(path.join(outDir, 'example.docx'))
  
    out.on('error', function (err) {
      console.log("Out error: " + err)
    });
  
    async.parallel([
      function (done) {
        out.on('close', function () {
          console.log('Finish to create a DOCX file.')
          done(null)
        });
  
        docx.generate(out);
      }
  
    ], function (err) {
      if (err) {
        console.log('async.parallel error: ' + err)
      }
    });
  }

  res.render('index', { title: 'Documento has bean created' });
});

module.exports = router;
